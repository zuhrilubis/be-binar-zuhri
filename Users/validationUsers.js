const { body } = require("express-validator");

const registrationSchema = [
  body("email")
    .notEmpty()
    .withMessage("Please enter the email")
    .isEmail()
    .withMessage("Must be correct email"),
  body("password")
    .notEmpty()
    .withMessage("Please enter the password")
    .matches(
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.* )(?=.*[^a-zA-Z0-9]).{8,}$/,
      "i"
    )
    .withMessage(
      "Password must be 8 character, include a number and a special character."
    ),
  body("userName")
    .notEmpty()
    .withMessage("Please enter the username")
    .isString()
    .withMessage("Must be character"),
];

const loginSchema = [
  body("userName")
    .notEmpty()
    .withMessage("Please enter the username")
    .isString()
    .withMessage("Must be character"),
  body("password")
    .notEmpty()
    .withMessage("Please enter the password")
    .matches(
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.* )(?=.*[^a-zA-Z0-9]).{8,}$/,
      "i"
    )
    .withMessage(
      "Password must be 8 character, include a number and a special character."
    ),
];

const updateBioSchema = [
  body("phoneNumber")
    .notEmpty()
    .withMessage("Please enter the phone number")
    .isInt()
    .withMessage("Must be number"),
  body("address")
    .notEmpty()
    .withMessage("Please enter the address")
    .isString()
    .withMessage("Must be character"),
  body("fullName")
    .notEmpty()
    .withMessage("Please enter the fullname")
    .isString()
    .withMessage("Must be character"),
];

// validation for game rule
const storeGameHistorySchema = [
  body("status")
    .notEmpty()
    .withMessage("Please enter the result")
    .custom((value) => {
      const validChoices = ["menang", "imbang", "kalah"];
      if (!validChoices.includes(value)) {
        throw new Error("Invalid choice, should be menang, imbang, or kalah.");
      }
      return true;
    }),
];

const createGameRoom = [
  body("roomName")
    .notEmpty()
    .withMessage("Please enter the room Name")
    .isString()
    .withMessage("Must be character"),
  body("playerOneChoice")
    .notEmpty()
    .withMessage("Please enter your choice")
    .custom((value) => {
      const validChoices = ["batu", "gunting", "kertas"];
      if (!validChoices.includes(value)) {
        throw new Error("Invalid choice, should be batu, gunting, or kertas.");
      }
      return true;
    }),
];

const enemyChoice = [
  body("playerTwoChoice")
    .notEmpty()
    .withMessage("Please enter your choice")
    .custom((value) => {
      const validChoices = ["batu", "gunting", "kertas"];
      if (!validChoices.includes(value)) {
        throw new Error("Invalid choice, should be batu, gunting, or kertas.");
      }
      return true;
    }),
];

const vsComputer = [
  body("playerOneChoice")
    .notEmpty()
    .withMessage("Please enter your choice")
    .custom((value) => {
      const validChoices = ["batu", "gunting", "kertas"];
      if (!validChoices.includes(value)) {
        throw new Error("Invalid choice, should be batu, gunting, or kertas.");
      }
      return true;
    }),
];

module.exports = {
  registrationSchema,
  loginSchema,
  updateBioSchema,
  storeGameHistorySchema,
  createGameRoom,
  enemyChoice,
  vsComputer,
};
