const express = require("express");
const ControlUsers = require("./controlUsers");
const RouteUsers = express.Router();
const authoMiddleware = require("../Middleware/authoMiddleware");
const protectionUser = require("../Middleware/protectMiddleware");
// const schemaValidation = require("../Middleware/schemaValidation");
// const dataValidation = require("../Users/validationUsers");

// untuk mendaftarkan user baru
RouteUsers.post(
  "/daftar",
  ControlUsers.storeNewUsers
);

// untuk mendapatkan data seluruh user
RouteUsers.get("/user", authoMiddleware, ControlUsers.getAllUsers);

// untuk melakukan login
RouteUsers.post(
  "/login",
  // dataValidation.loginSchema,
  // schemaValidation,
  ControlUsers.verifyLogin
);

// untuk update biodata user
RouteUsers.put(
  "/updateBio",
  authoMiddleware,
  // protectionUser,
  // dataValidation.updateBioSchema,
  // schemaValidation,
  ControlUsers.updateUserBio
);

// untuk mendapatkan detail data single user
RouteUsers.get(
  "/userIdBio",
  authoMiddleware,
  // protectionUser,
  ControlUsers.getOneUser
);

// untuk menambahkan history game user
RouteUsers.post(
  "/addGameHistory/:userId",
  authoMiddleware,
  protectionUser,
  // dataValidation.storeGameHistorySchema,
  // schemaValidation,
  ControlUsers.storeGameHistory
);

// mendapatakan detail history game dari single user
RouteUsers.get(
  "/detiles/gameHistory/:userId",
  authoMiddleware,
  protectionUser,
  ControlUsers.getOneUserGameHistory
);

// API Tambahan
// 1. API Create Room
RouteUsers.post(
  "/createRoom",
  authoMiddleware,
  // dataValidation.createGameRoom,
  // schemaValidation,
  ControlUsers.createGameRoom
);

// 2. mendapatkan data seluruh room
RouteUsers.get("/allGameRoom", authoMiddleware, ControlUsers.getAllGameRooms);

// 3. mendapatkan single room detail
RouteUsers.get(
  "/roomDetiles/:roomId",
  authoMiddleware,
  ControlUsers.getOneRoomGame
);

// 4. mengupdate single room game
RouteUsers.put(
  "/roomUpdate/:roomId",
  authoMiddleware,
  // dataValidation.enemyChoice,
  // schemaValidation,
  ControlUsers.updateOneRoomGame
);

// 5. mendapatkan History game per user
RouteUsers.get(
  "/allGameHistory",
  authoMiddleware,
  ControlUsers.getSingleGameHistory
);

// 6. API untul Player VS Computer
RouteUsers.post(
  "/vsComputer",
  authoMiddleware,
  // dataValidation.vsComputer,
  // schemaValidation,
  ControlUsers.createRoomPlayerVsComputer
);

module.exports = RouteUsers;
