"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasOne(models.UserBio, { foreignKey: "userId" });
      User.hasMany(models.GameHistory, { foreignKey: "userId" });
      User.hasMany(models.GameRoom, {
        foreignKey: "playerOneId",
        as: "playerOne",
      });
      User.hasMany(models.GameRoom, {
        foreignKey: "playerTwoId",
        as: "playerTwo",
      });
    }
  }
  User.init(
    {
      userName: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
